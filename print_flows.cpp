
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for computing and printing network flows.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/format.hpp>

#ifdef WN_USER
#include "master.h"
#else
#include "default/master.h"
#endif

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] ) {

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the data.
  //============================================================================

  wn_user::libnucnet_data my_libnucnet_data( v_map );

  //============================================================================
  // Create flow_computer.  Set rates.
  //============================================================================

  wn_user::flow_computer my_flow_computer( v_map );

  //============================================================================
  // Get the zones to study.  Sort them first.
  //============================================================================

  Libnucnet__setZoneCompareFunction(
    my_libnucnet_data.getNucnet(),
    (Libnucnet__Zone__compare_function) nnt::zone_compare_by_first_label
  );

  std::vector<nnt::Zone> zones = my_libnucnet_data.getVectorOfZones();

  //============================================================================
  // Compute flows.
  //============================================================================

  wn_user::flow_map_vector_t flows = my_flow_computer( zones );

  //============================================================================
  // Loop on zones.
  //============================================================================

  for( size_t i = 0; i < flows.size(); i++ )
  {

    if( zones[i].hasProperty( nnt::s_TIME ) )
    {
      std::cout <<
        boost::format(
          "time(s) = %12.4e t9 = %12.4e rho(g/cc) = %12.4e\n"
        )
           % zones[i].getProperty<std::string>( nnt::s_TIME )
           % zones[i].getProperty<std::string>( nnt::s_T9 )
           % zones[i].getProperty<std::string>( nnt::s_RHO );
    }
    else
    {
      std::cout <<
        boost::format(
          "zone (%lu,%lu,%lu) t9 = %12.4e rho(g/cc) = %12.4e\n"
        )
           % Libnucnet__Zone__getLabel( zones[i].getNucnetZone(), 1 )
           % Libnucnet__Zone__getLabel( zones[i].getNucnetZone(), 2 )
           % Libnucnet__Zone__getLabel( zones[i].getNucnetZone(), 3 )
           % zones[i].getProperty<std::string>( nnt::s_T9 )
           % zones[i].getProperty<std::string>( nnt::s_RHO );
    }

    std::cout <<
      boost::format("\n\t\t\tReaction\t\t\t  Forward     Reverse     Net\n" );
      
    std::cout <<
      boost::format(
        "=======================================================   =========   =========   =========\n"
      );

    BOOST_FOREACH( wn_user::flow_map_t::value_type& t, flows[i] )
    {

      std::cout <<
        boost::format( "%-55s%12.3e%12.3e%12.3e\n" ) %
        t.first %
        t.second.first %
        t.second.second %
        ( t.second.first - t.second.second );
    }

    std::cout << std::endl;

  }

  //============================================================================
  // Done.
  //============================================================================

  return EXIT_SUCCESS;

}
